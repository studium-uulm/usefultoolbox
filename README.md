# UsefulToolbox

A super useful toolbox to help in everyday szenarios.




## Installation

You can choose one of two options:

- Build the project yourself from the files in this repo
- install one of the **prebuilt APKs** from `/releases/`

## FAQ

#### How do I run the project?

We recommend using JetBrains 'Android Studio' for running the project. An emulator or connected Android device is also neccessary

#### Why was this created?

This was a part of a project within the Bachelor of Computer science at the University of Ulm, Germany

  