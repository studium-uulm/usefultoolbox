package org.uuuk.usefultoolbox.ui;

import android.media.MediaPlayer;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import org.uuuk.usefultoolbox.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HorrorMeditation#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HorrorMeditation extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    MediaPlayer mp;
    private Button start;
    private Button stop;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    ImageView winkelSpinne;

    public HorrorMeditation() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HorrorMeditation.
     */
    // TODO: Rename and change types and number of parameters
    public static HorrorMeditation newInstance(String param1, String param2) {
        HorrorMeditation fragment = new HorrorMeditation();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        //mp= MediaPlayer.create(getContext(),R.raw.horrormed);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_horror_meditation, container, false);
        // Inflate the layout for this fragment
        start = view.findViewById(R.id.horrorMedStart);
        stop = view.findViewById(R.id.horrorMedStop);
        winkelSpinne = view.findViewById(R.id.winkelSpinne);
        LinearLayout startStopButtonsLayout = view.findViewById(R.id.horror_startStopBtnsLinearLayout);

        start.setOnClickListener(v -> {
            toggleButtons(true);
            mp= MediaPlayer.create(getContext(),R.raw.horrormed);
            mp.start();
            //startStopButtonsLayout.setVisibility(View.GONE);
            winkelSpinne.setVisibility(View.VISIBLE);

            int offset = 0;
            int flickerRate = 100;
            int firstJumpScareMs = 76000 + offset;
            int secondJumpScareMS = 100922 + offset;
            int thirdJumpScareMS = 139431 + offset;

            (new Handler()).postDelayed(this::turnOffSpider, 100);

            (new Handler()).postDelayed(this::turnOnSpider, firstJumpScareMs);// jump scare piano clusters + 1st organ motif
            (new Handler()).postDelayed(this::turnOffSpider, firstJumpScareMs + 1*flickerRate);
            (new Handler()).postDelayed(this::turnOnSpider, firstJumpScareMs + 2*flickerRate);
            (new Handler()).postDelayed(this::turnOffSpider, firstJumpScareMs + 3*flickerRate);

            (new Handler()).postDelayed(this::turnOnSpider, secondJumpScareMS);// jump scare BD
            (new Handler()).postDelayed(this::turnOffSpider, secondJumpScareMS + 1*flickerRate);
            (new Handler()).postDelayed(this::turnOnSpider, secondJumpScareMS + 2*flickerRate);
            (new Handler()).postDelayed(this::turnOffSpider, secondJumpScareMS + 3*flickerRate);

            (new Handler()).postDelayed(this::turnOnSpider, thirdJumpScareMS);// jump scare string trem.
            (new Handler()).postDelayed(this::turnOffSpider, thirdJumpScareMS + 1*flickerRate);
            (new Handler()).postDelayed(this::turnOnSpider, thirdJumpScareMS + 2*flickerRate);
            (new Handler()).postDelayed(this::turnOffSpider, thirdJumpScareMS + 3*flickerRate);
        });

        stop.setOnClickListener(v -> {
            mp.stop();
            toggleButtons(false);
        });
        return view;
    }

    private void toggleButtons(boolean setPlaying){
        start.setVisibility(setPlaying ? View.GONE : View.VISIBLE);
        stop.setVisibility(setPlaying ? View.VISIBLE : View.GONE);
    }

    public void turnOnSpider(){
        winkelSpinne.setVisibility(View.VISIBLE);
    }

    public void turnOffSpider(){
        winkelSpinne.setVisibility(View.GONE);
    }

}