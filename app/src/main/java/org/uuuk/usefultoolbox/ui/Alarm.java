package org.uuuk.usefultoolbox.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.graphics.drawable.GradientDrawable;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.material.snackbar.Snackbar;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;

import org.uuuk.usefultoolbox.R;

import java.util.Date;


public class Alarm extends Fragment {

    boolean activeAlarm = false;
    int hour = -1;
    int minute = -1;

    boolean[] activeDays = new boolean[7];
    Button[] activeDaysButtons = new Button[7];

    Button btnSet;
    Button btnIsSet;
    TextView lblTime;

    public Alarm() {
        // Required empty public constructor
    }

    private void setActiveDay(int id) {
        if(activeAlarm) return;
        activeDays[id] = !activeDays[id];
        refreshDayButtons();
    }
    private void refreshDayButtons(){
        for(int i = 0; i < activeDays.length; i++){
            activeDaysButtons[i].setBackgroundColor(activeDays[i] ? 0xFF6200ee : 0xFF333333);
        }
    }

    private void setTime(int h, int m){
        lblTime.setText((h <= 9 ? "0" : "") + h + " : " + (m <= 9 ? "0" : "") + m);
        hour = h;
        minute = m;
    }

    private void setAlarm(){
        if(hour < 0 || minute < 0){
            Snackbar.make(getView(), "please set a time first",7000).show();
            return;
        }
        Snackbar.make(getView(), "your alarm was set!",7000).show();
        showSetBtn(false);

        // set random time
        Date date = new Date();
        int h = date.getHours();
        int m = date.getMinutes();

        int hdiff = (int) (hour - h + (Math.round(Math.random() * 5)));
        if(hdiff < 0) hdiff = hdiff + 24;

        int period = hdiff * 1000 * 60 * 60 + m * 1000 * 60;

        new AlarmTimer(period, this).start();
    }

    public void clearAlarm(){
        activeAlarm = false;
        showSetBtn(true);
    }

    public void showSetBtn(boolean show){
        btnSet.setVisibility(show ? View.VISIBLE : View.GONE);
        btnIsSet.setVisibility(!show ? View.VISIBLE : View.INVISIBLE);
    }

    public void onViewCreated(View view, Bundle savedInstanceState) {

        btnSet = getView().findViewById(R.id.btn_set);
        btnIsSet = getView().findViewById(R.id.btn_isSet);
        lblTime = getView().findViewById(R.id.lbl_time);

        int[] dayBtnIds = new int[]{
                R.id.btn_days_mon,
                R.id.btn_days_tue,
                R.id.btn_days_wed,
                R.id.btn_days_thu,
                R.id.btn_days_fri,
                R.id.btn_days_sat,
                R.id.btn_days_sun,
        };

        for(int i = 0; i < dayBtnIds.length; i++){
            final int ind = i;
            activeDaysButtons[i] = getView().findViewById(dayBtnIds[i]);
            activeDaysButtons[i].setOnClickListener((e) -> setActiveDay(ind));
        }

        lblTime.setOnClickListener((e) -> {
            if(activeAlarm) return;
            new TimePickerDialog(getContext(), (a,b,c) -> {
                this.setTime(b,c);},
                4,20,true).show();});

        btnSet.setOnClickListener((e) -> setAlarm());
        refreshDayButtons();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_alarm, container, false);
    }
}

class AlarmTimer extends Thread {
    private int millis;
    private Alarm view;

    public AlarmTimer(int millis, Alarm view){
        this.millis = millis;
        this.view = view;
    }

    public void onAlarm(){
        view.getActivity().runOnUiThread(() -> {
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
            Ringtone r = RingtoneManager.getRingtone(view.getActivity().getApplicationContext(), notification);
            r.play();
            view.clearAlarm();
            new AlertDialog.Builder(view.getContext())
                    .setTitle("AAALARM!!")
                    .setMessage("")
                    .setOnDismissListener((e) -> r.stop()).show();
        });
    }

    public void run() {
        try {
            Thread.sleep(millis);
            onAlarm();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}