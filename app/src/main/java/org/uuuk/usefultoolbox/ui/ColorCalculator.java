package org.uuuk.usefultoolbox.ui;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.uuuk.usefultoolbox.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ColorCalculator#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ColorCalculator extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private Button btn1;
    private Button btn2;
    private Button btnRes;

    private TextView[] opBtns = new TextView[4];

    private View seperator;

    private int color1 = 0xFF333333;
    private int color2 = 0xFF333333;

    private boolean firstColor = false;
    private int operation = 0; // 0:add, 1:sub, 2:mult, 3:div

    public static final int[] COLORS = new int[]{
            0xFFFF0000,
            0xFFFFFF00,
            0xFF00FF00,
            0xFF00FFFF,
            0xFF0000FF,
            0xFFFF00FF,
            0xFFFF0000,
    };

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public ColorCalculator() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ColorCalculator.
     */
    // TODO: Rename and change types and number of parameters
    public static ColorCalculator newInstance(String param1, String param2) {
        ColorCalculator fragment = new ColorCalculator();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        System.out.println("create gradient");
        GradientDrawable gradientDrawable = new GradientDrawable(
                GradientDrawable.Orientation.LEFT_RIGHT,COLORS);

        View colorPicker =  getView().findViewById(R.id.img_pickerBackground);
        ImageView colorPickerDot =  getView().findViewById(R.id.img_colorPickerDot);
        TextView colorPickerCode =  getView().findViewById(R.id.lbl_colorPickerCode);
        colorPicker.setBackground(gradientDrawable);

        // color buttons
        btn1 =  getView().findViewById(R.id.btn_Color1);
        btn1.setOnClickListener((e)-> setActiveButton(true));
        btn2 =  getView().findViewById(R.id.btn_Color2);
        btn2.setOnClickListener((e)-> setActiveButton(false));
        btnRes =  getView().findViewById(R.id.btn_ColorResult);
        seperator =  getView().findViewById(R.id.view_seperator);


        // operation buttons
        opBtns[0] = getView().findViewById(R.id.lbl_op_plus);
        opBtns[0].setOnClickListener((e) -> setOperation(0));
        opBtns[1] = getView().findViewById(R.id.lbl_op_minus);
        opBtns[1].setOnClickListener((e) -> setOperation(1));
        opBtns[2] = getView().findViewById(R.id.lbl_op_mult);
        opBtns[2].setOnClickListener((e) -> setOperation(2));
        opBtns[3] = getView().findViewById(R.id.lbl_op_div);
        opBtns[3].setOnClickListener((e) -> setOperation(3));

        colorPicker.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN || event.getAction() == MotionEvent.ACTION_MOVE){
                colorPickerDot.setTranslationX(event.getX());
                colorPickerDot.setTranslationY(event.getY());

                Color chosen = calculateClickColor(view.getWidth(), view.getHeight(), Math.round(event.getX()), Math.round(event.getY()));
                // setting indicator color
                colorPickerCode.setText(String.format("#%06X", (0xFFFFFF & chosen.toArgb())));

                // drawing the background of the button
                calculateColor(chosen.toArgb());
            }
            return true;
        });

        calculateColor();
    }

    private void setOperation(int op){
        operation = Math.max(0,Math.min(3,op));

        for(TextView v : opBtns){
            v.setTextColor(0xFF7A7A7A);
        }
        opBtns[operation].setTextColor(0xFFFFFFFF);

        calculateColor();
    }

    private void setActiveButton(boolean firstButton){
        this.firstColor = firstButton;
        if(seperator != null){
            System.out.println("setting active button");
            ViewGroup.LayoutParams layoutParams = seperator.getLayoutParams();
            layoutParams.width = firstColor ? 55*4 : ViewGroup.LayoutParams.MATCH_PARENT;
            seperator.setLayoutParams(layoutParams);
        }
    }

    private void calculateColor(int color){
        if(btn1 == null || btn2 == null) return;

        if(firstColor) color1 = color;
        else color2 = color;

        (firstColor ? btn1 : btn2).setBackgroundColor(color);
        calculateColor();
    }

    private void calculateColor(){
        if(btn1 == null || btn2 == null || btnRes == null) return;

        Color c1 = Color.valueOf(color1);
        Color c2 = Color.valueOf(color2);

        Color res = Color.valueOf(
                calcColorField(Math.round(c1.red()*255), Math.round(c2.red()*255)),
                calcColorField(Math.round(c1.green()*255), Math.round(c2.green()*255)),
                calcColorField(Math.round(c1.blue()*255), Math.round(c2.blue()*255)),
                1
        );
        btnRes.setBackgroundColor(res.toArgb());
    }

    private float calcColorField(int v1, int v2){
        int res = v1;
        if(operation == 0) res = v1 + v2;
        if(operation == 1) res = v1 - v2;
        if(operation == 2) res = v1 * v2;
        if(operation == 3) res = v1 / Math.max(1,v2);

        return (Math.max(0,res) % 255) / 255.0f;
    }

    private Color calculateClickColor(int width, int height, int x, int y){
        int index = x / (width/COLORS.length);
        float frac = x / (width/COLORS.length + 0.0f) - index;
        index = Math.max(0, Math.min(COLORS.length - 2, index));

        float l = 1 - (y/(height + 0.0f));

        Color c1 = Color.valueOf(COLORS[index]);
        Color c2 = Color.valueOf(COLORS[index + 1]);

        float r = c1.red() + (c2.red() - c1.red()) * frac;
        float g = c1.green() + (c2.green() - c1.green()) * frac;
        float b = c1.blue() + (c2.blue() - c1.blue()) * frac;

        return Color.valueOf(r*l,g*l,b*l,1);
    }

        @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_color_calculator, container, false);
    }
}