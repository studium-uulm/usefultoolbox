package org.uuuk.usefultoolbox.ui;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.ToneGenerator;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.pdf417.encoder.BarcodeMatrix;

import org.uuuk.usefultoolbox.MainActivity;
import org.uuuk.usefultoolbox.R;

import java.io.IOException;

public class BarcodeReader extends Fragment {

    private SurfaceView surfaceView;
    private BarcodeDetector barcodeDetector;
    private CameraSource cameraSource;
    private Button barcodeCard;


    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public BarcodeReader() {
        // Required empty public constructor
    }

    public static BarcodeReader newInstance(String param1, String param2) {
        BarcodeReader fragment = new BarcodeReader();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    // 'init'-method based on the work published by Hari Lee published at:
    // https://medium.com/analytics-vidhya/creating-a-barcode-scanner-using-android-studio-71cff11800a2
    private void init(View view) {

        barcodeDetector = new BarcodeDetector.Builder(view.getContext())
                .setBarcodeFormats(Barcode.ALL_FORMATS)
                .build();

        cameraSource = new CameraSource.Builder(view.getContext(), barcodeDetector)
                .setRequestedPreviewSize(1920, 1080)
                .setAutoFocusEnabled(true) //you should add this feature
                .build();

        surfaceView.getHolder().addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                try {
                    if (ActivityCompat.checkSelfPermission(view.getContext(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        cameraSource.start(surfaceView.getHolder());
                    } else {
                        ActivityCompat.requestPermissions(getActivity(), new
                                String[]{Manifest.permission.CAMERA}, 201);
                    }

                } catch (IOException e) {
                }
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                cameraSource.stop();
            }
        });


        barcodeDetector.setProcessor(new Detector.Processor<Barcode>() {
            @Override
            public void receiveDetections(Detector.Detections<Barcode> detections) {

                final SparseArray<Barcode> barcodes = detections.getDetectedItems();
                if (barcodes.size() > 0) {
                    System.out.println("recognised barcode");
                    new BarcodeSpeaker(barcodes.valueAt(0), barcodeCard, getActivity()).start();
                }
            }

            @Override
            public void release() {
                Toast.makeText(getContext(), "The scanner has been paused", Toast.LENGTH_SHORT).show();
            }
        });
    }



    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        surfaceView = view.findViewById(R.id.cam_barcodeCamera);
        barcodeCard = view.findViewById(R.id.btn_ScannerCard);
        init(view);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_barcode_reader, container, false);
    }
}

class BarcodeSpeaker extends Thread {
    private final Barcode _barcode;
    private Activity _activity;
    private Button _card;

    private static boolean isRunning = false;

    public BarcodeSpeaker(Barcode code, Button card, Activity activity) {
        _barcode = code;
        _activity = activity;
        _card = card;
    }

    private void showCard(boolean visible){
        _activity.runOnUiThread(() -> _card.setVisibility(visible ? View.VISIBLE : View.INVISIBLE));
    }

    private void setValue(boolean black){
        _activity.runOnUiThread(() -> {
            _card.setText(black ? "BLACK" : "WHITE");
            _card.setBackgroundColor(black ? 0xFF000000 : 0xFFFFFFFF);
            _card.setTextColor(black ? 0xFFFFFFFF : 0xFF000000);
        });
    }

    public void run() {
        //TODO in der Theorie ist das nicht ganz sicher so
        if(isRunning ) return;
        isRunning = true;
        try {
            MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
            BitMatrix bitMatrix = multiFormatWriter.encode(_barcode.rawValue, BarcodeFormat.CODE_128, 200, 1);
            showCard(true);
            boolean started = false;
            for (int i = 0; i < 200; i++) {
                boolean val = bitMatrix.get(i, 0);
                if(val) started = true;
                if(started){
                    setValue(val);
                    Thread.sleep(300);
                }
            }
            showCard(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        isRunning = false;
    }
}