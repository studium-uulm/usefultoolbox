package org.uuuk.usefultoolbox.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import org.uuuk.usefultoolbox.R;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link UnreliableTodoList#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UnreliableTodoList extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private ListView todoItems;
    private FloatingActionButton addFAB;

    //private ArrayList<String> items;
    private ArrayAdapter<String> itemsAdapter;

    public UnreliableTodoList() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment UnreliableTodoList.
     */
    // TODO: Rename and change types and number of parameters
    public static UnreliableTodoList newInstance(String param1, String param2) {
        UnreliableTodoList fragment = new UnreliableTodoList();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    public void onCreate(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_unreliable_todo_list, container, false);

        todoItems = view.findViewById(R.id.todoItems);
        addFAB = view.findViewById(R.id.floatingActionButton);
        addFAB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onFABclick(v);
            }
        });

        ArrayList<String> items = new ArrayList<String>();
        itemsAdapter = new ArrayAdapter<String>(view.getContext(), R.layout.todo_item, items);
        todoItems.setAdapter(itemsAdapter);
        items.add("Feed the Kids");
        items.add("Test the App");

        todoItems.setOnItemClickListener((a,b,c,d) -> {
            itemsAdapter.remove(items.get(c));
        });

        new TodoThread(this).start();
        return view;
    }

    public void removeRandomItem(){
        if(itemsAdapter.getCount() > 0) {
            int randomIndex = Math.min(itemsAdapter.getCount(), (int) Math.round(Math.random() * itemsAdapter.getCount()));
            itemsAdapter.remove(itemsAdapter.getItem(randomIndex));
        }
    }

    public void onFABclick(View v){
        final EditText taskEditText = new EditText(this.getContext());
        AlertDialog dialog = new AlertDialog.Builder(this.getContext())
                .setTitle("Add new Item")
                .setMessage("What do you want to add?")
                .setView(taskEditText)
                .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String toAdd = String.valueOf(taskEditText.getText());
                        itemsAdapter.add(toAdd);
                    }
                })
                .setNegativeButton("Cancel", null)
                .create();
        dialog.show();
    }
}

class TodoThread extends Thread {
    private UnreliableTodoList view;

    public TodoThread(UnreliableTodoList view){
        this.view = view;
    }

    public void run() {
        try {
            while (true) {
                Thread.sleep(30 * 1000);
                // remove random item in 20 percent of iterations
                if (Math.random() < 0.2) {
                    view.getActivity().runOnUiThread(() -> {
                        view.removeRandomItem();
                    });
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}